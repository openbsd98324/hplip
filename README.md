# hplip


## Ubuntu 

3.21.12 dfsg0 1

## Setup

HP Linux Imaging and Printing

hplip-3.23.5.tar.gz

````
Enter number 0...12 (q=quit) ?0

Distro set to: Manjaro Linux 0.0

Initializing. Please wait...


INSTALLATION NOTES
------------------
Enable the universe/multiverse repositories. Also be sure you are using the Manjaro "Main" Repositories. See: https://wiki.manjaro.org/index.php/Arch_User_Repository for more information.  Disable the CD-ROM/DVD source if you do not have the Manjaro installation media inserted in the drive.

Please read the installation notes. Press <enter> to continue or 'q' to quit: 


RUNNING PRE-INSTALL COMMANDS
----------------------------
OK


RUNNING HPLIP LIBS REMOVE COMMANDS
----------------------------------
None
OK


MISSING DEPENDENCIES
--------------------
Following dependencies are not installed. HPLIP will not work if all REQUIRED dependencies are not installed and some of the HPLIP features will not work if OPTIONAL dependencies are not installed.
Package-Name         Component            Required/Optional   
cups-devel           base                 REQUIRED            
cups-ddk             base                 OPTIONAL            
sane                 scan                 REQUIRED            
sane-devel           scan                 REQUIRED            
xsane                scan                 OPTIONAL            
scanimage            scan                 OPTIONAL            
libnetsnmp-devel     network              REQUIRED            
python3-notify2      gui_qt5              OPTIONAL            
python3-pyqt4-dbus   gui_qt4              OPTIONAL            
python3-pyqt4        gui_qt4              REQUIRED            
python3-pyqt5-dbus   gui_qt5              OPTIONAL            
python3-pyqt5        gui_qt5              REQUIRED            
python3-dbus         fax                  REQUIRED            
python3-pil          scan                 OPTIONAL            
python3-reportlab    fax                  OPTIONAL            
Do you want to install these missing dependencies (y=yes*, n=no, q=quit) ? 


INSTALL MISSING REQUIRED DEPENDENCIES
-------------------------------------
note: Installation of dependencies requires an active internet connection.
warning: Missing REQUIRED dependency: cups-devel (CUPS devel- Common Unix Printing System development files)
warning: This installer cannot install 'cups-devel' for your distro/OS and/or version.
error: Installation cannot continue without this dependency. Please manually install this dependency and re-run this installer.


The only solution is to install Raspberry Pi ARM64 Beta Testing Operating System which is based of Debian Buster ARM64 and then use Backports via APT to install a newer release of hplip:

https://downloads.raspberrypi.org/raspi ... 2021-04-09

https://packages.debian.org/buster-backports/hplip

Code: Select all

 apt-cache policy hplip
hplip:
  Installed: 3.20.9+dfsg0-4~bpo10+1
  Candidate: 3.20.9+dfsg0-4~bpo10+1
  Version table:
 *** 3.20.9+dfsg0-4~bpo10+1 100
        100 http://deb.debian.org/debian buster-backports/main arm64 Packages
        100 /var/lib/dpkg/status
     3.18.12+dfsg0-2 500
        500 http://deb.debian.org/debian buster/main arm64 Packages



````
